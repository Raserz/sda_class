
#Public and Private
#def print_something(self):
    #print(f"public")


#def __print_anotherthing(self):
    #print(f"private")


#Student().print_something()
#Student().__print_anotherthing()


class Student:
    def __init__(self, name: str = "John Doe", course: str = "Basic Science", gpa: float = 0.00 ) -> None:
        self.__name = name
        self.__course = course
        self.__validate_gpa_is_float(gpa)
        self.__gpa =gpa


    def __validate_gpa_is_float(self, gpa):
        #check if gpa is numeric, if not numeric raise an error
        if not isinstance(self.__gpa, (int, float)):
            raise TypeError(f"{gpa} should be float")



    def __validate_gpa_is_within_limit(self, number):
        if number < 1 or number > 5:
            raise ValueError(f"{number} is not valid gpa")


    def class_of_student_grade(self):
        self.__validate_gpa_is_within_limit(self, gpa):
        if self.__gpa >= 4.5:
            return "Excellent"
        elif self.__gpa >=3.5 and self.__gpa < 4.5:
            return "Good"
        elif self.__gpa >= 2.5 and self.__gpa < 3.5:
            return "Average"
        else:
            return "Try Harder"









